const { RTMClient } = require('@slack/client');
var ENV = process.env.NODE_ENV || 'development';
var config = require('../config');
var twitter = require('./twitter.js');

// The client is initialized and then started to get an active connection to the platform
const rtm = new RTMClient(config.slack.token);

//function test messages if cotaining go
var regexCheck = function (someText) {
    return new RegExp('\\b' + config.basicMessage + '\\b').test(someText.toString().toLowerCase())
}

rtm.start();

rtm.on('message', (message) => {

    //Skip messages that are from any channels except our channel or doesnot contain go
    if (message.channel != config.slack.conversationId || !(regexCheck(message.text))) {
        return;
    }

    //fetching tweets from twitter
    twitter.fetchTweets();

    // Log the message to server
    console.log(`Zappy now listened on (channel:${message.channel}) and (user: ${message.user}) sent: ${message.text}`);
});

module.exports = {
    regexCheck: regexCheck
}