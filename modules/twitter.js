var Twitter = require('twitter');
var tweetsCtrl = require('../controllers/tweets');
var config = require('../config');

//application authnticate user 
var client = new Twitter({
  consumer_key: config.twitter.consumerKey,
  consumer_secret: config.twitter.consumerSecretKey,
  access_token_key: config.twitter.accessToken,
  access_token_secret: config.twitter.accessTokenSecret
});

var params = { screen_name: 'nodejs' };

//function fetch tweeets from home timeline 
var fetchTweets = function () {

  client.get('statuses/home_timeline', params, function (error, tweets, response) {
    
    //handle error 
    if (error) {
      console.log("Error occured: " + JSON.stringify(error))
      return null;
    }
    else {
      console.log("Tweets fetched Number: " + tweets.length);

      // save tweets to mongo
      tweetsCtrl.saveTweets(tweets);
      return tweets.length;
    }
  });
}

module.exports = {
  fetchTweets: fetchTweets
}
