FROM node:8


# Set up app directory
WORKDIR /usr/src/zappy

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# Expose the server to the host machine
EXPOSE 4000

CMD [ "npm", "start" ]
