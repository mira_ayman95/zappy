chai = require('chai');
assert = chai.assert;
expect = chai.expect;
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);
chai.should();
chai.config.includeStack = true;
var supertest = require("supertest");
var config = require('../../config');
var server = supertest.agent(config.host);
var helper = require('../test-helper.js');
let mongoose = require('mongoose');
let Tweets = require('../../models/tweets');

// for (g = 0; g < 1; g++) {

describe("Tweets API", function () {

    this.timeout(10000);

    // for(x=0; x<50; x++){

    describe("Get /api/tweets", function () {

        it("Should return 5 tweets from feeds", function (done) {
            server
                .get('/api/tweets/2/5')
                .expect("Content-type", /json/)
                .expect(200)
                .end(function (err, res) {
                    // if (res.body) console.log(res.body);
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('tweets');
                    expect(res.body.tweets).to.be.array;
                    expect(res.body.tweets).to.have.length(5);
                    done();
                });
        });

        it("Should return 3 tweets from feeds", function (done) {
            server
                .get('/api/tweets/1/3')
                .expect("Content-type", /json/)
                .expect(200)
                .end(function (err, res) {
                    // if (res.body) console.log(res.body);
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('tweets');
                    expect(res.body.tweets).to.be.array;
                    expect(res.body.tweets).to.have.length(3);
                    done();
                });
        });

        it("Should return 0 tweets from feeds, pagenumber is exceed record number ", function (done) {
            server
                .get('/api/tweets/1000/5')
                .expect("Content-type", /json/)
                .expect(400)
                .end(function (err, res) {
                    // if (res.body) console.log(res.body);
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('error');
                    expect(res.body.error).to.be.string;
                    expect(res.body.error).to.equal("No more tweets in database.");
                    done();
                });
        });
    });

    describe("Get /anyroute", function () {
        it("Should not access this route, route not found!", function (done) {
            server
                .get('/' + helper.generate('lalpha', 5))
                .expect("Content-type", /json/)
                .expect(404)
                .end(function (err, res) {
                    // if (res.body) console.log(res.body);
                    res.should.have.status(404);
                    done();
                });
        });
    });
});
// }