var helper = require('../test-helper.js');
var RMTClient = require('../../modules/RMTClient');
var chai = require('chai');
assert = chai.assert;
chai.config.includeStack = true;

for (g = 0; g < 1; g++) {
    describe("RMTClient Module", function () {

        this.timeout(10000);

        // for(x=0; x<50; x++){

        describe("Test Regex Check function", function () {

            it("Should return false, go didnot included in sentence!", function () {
                var isValid = RMTClient.regexCheck(helper.generate('lalpha', 5) + " " + helper.generate('lalpha', 5));
                assert.equal(isValid, false);
            });

            it("Should return false, go included in sentence but with no spaces in the begining of the word!", function () {
                var isValid = RMTClient.regexCheck(helper.generate('lalpha', 5) + "go " + helper.generate('lalpha', 5));
                assert.equal(isValid, false);
            });

            it("Should return false, go included in sentence but with no spaces at the end of the word!", function () {
                var isValid = RMTClient.regexCheck(helper.generate('lalpha', 5) + " go" + helper.generate('lalpha', 5));
                assert.equal(isValid, false);
            });

            it("Should return true, go included in the middle sentence", function () {
                var isValid = RMTClient.regexCheck(helper.generate('lalpha', 5) + " go " + helper.generate('lalpha', 5));
                assert.equal(isValid, true);
            }); 

            it("Should return true, go included in the begining sentence", function () {
                var isValid = RMTClient.regexCheck("go "+helper.generate('lalpha', 5) + " go " + helper.generate('lalpha', 5));
                assert.equal(isValid, true);
            }); 

            it("Should return true, go included in the end of sentence", function () {
                var isValid = RMTClient.regexCheck(helper.generate('lalpha', 5) +" " + helper.generate('lalpha', 5)+ " go");
                assert.equal(isValid, true);
            }); 

            it("Should return true, go included in the sentence capitaized ", function () {
                var isValid = RMTClient.regexCheck(helper.generate('lalpha', 5) + " GO " + helper.generate('lalpha', 5));
                assert.equal(isValid, true);
            }); 

            it("Should return true, go duplicate in the sentence  ", function () {
                var isValid = RMTClient.regexCheck(" GO " +helper.generate('lalpha', 5) + " GO " + helper.generate('lalpha', 5));
                assert.equal(isValid, true);
            }); 

        });

    });
}