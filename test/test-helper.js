module.exports = {
    generate: function (opt, length) {

        var text = "";
        var possible = [];
        
        switch (opt) {
            case 'lalpha':
                possible = ["abcdefghijklmnopqrstuvwxyz"];
                break;
            case 'ualpha':
                possible = ["ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
                break;
            case 'all-alpha':
                possible = [
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                    "abcdefghijklmnopqrstuvwxyz"];
                break;
            case 'numeric':
                possible = ["0123456789"]
                break;
            case 'mix':
                var possible = [
                    "@$!%*?&",
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                    "abcdefghijklmnopqrstuvwxyz",
                    "0123456789"
                ];
                break;
            default:
                return false;
        }
        var x = 0;
        for (var i = 0; i < length; i++) {
            if (x == (possible.length)) { x = 0; }
            text += possible[x].charAt(Math.floor(Math.random() * possible[x].length));
            x++;
        }
        return text;
    }
}