'use strict'
module.exports = function (app) {

	var nameSpace = '/api';
    var tweets              = require('./routes/tweets');
	app.use(nameSpace, tweets);
}
	
