var express      = require('express');
var router       = express.Router();
var tweetsCtrl     = require('../../controllers/tweets');

router.route('/tweets/:page/:records').get(tweetsCtrl.getAllTweets())

module.exports = router;