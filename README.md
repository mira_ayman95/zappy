# Zappy

This project was generated with [Nodejs](https://github.com/nodejs/node) version 8.9.4.

## Configure app
To configure your app you should go to /config/envs/development.json and change data set to your data
So according to your slack account change with 
"slack": {
		"token": "xoxp-XXXXXXXXX",
		"conversationId": "CGQUQ7KDJ"
	}

-   token with your auth token 
-   channel that zappy will listen change conversationId to your channel id 

According to your twitter account get your data and change with 
"twitter": {
		"consumerKey": "XXXXXX",
		"consumerSecretKey": "XXXXXXXX",
		"accessToken": "XXXX",
		"accessTokenSecret": "XXXXXX"
	}

## Development server

Run `nodemon app.js` for a dev server. The app will automatically reload if you change any of the source files.

Once tha app runing you can  Navigate to `http://localhost:4000/api/tweets/0/1` 
you can get two response :

-   `{ error: "No more tweets in database." }` 
-   `{ tweets: [list of tweets] }`

If you got first one will show this means you have no tweets in DB 
If you got second one Congrats! the app running.

## Running unit tests

Run `npm test` to execute the unit tests via [mocha](https://github.com/mochajs/mocha).