let envs = {
    production: 'production',
    development: 'development'
}
let currentEnv = process.env.NODE_ENV || envs.development;

let config = require(`./env/${currentEnv}.json`);
  
config.envs = envs;
config.currentEnv = currentEnv;

console.log(`===================== CONFIG [${config.currentEnv}] =====================`);

module.exports = config;