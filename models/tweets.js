var mongoose = require('mongoose');
var dateFormat = require('dateformat');

var Tweet = mongoose.Schema({
	created_at: { type: String, required: true },
	id: { type: Number, required: true },
	id_str: { type: String, required: true },
	text: { type: String, required: true },
	user: {
		username: { type: String, required: true },
		name: { type: String, required: true },
		profileImg: { type: String, required: true }
	},
	media: { type: Array, required: false },
	retweet_count: { type: Number, required: false },
	favorite_count: { type: Number, required: false },
	reply_count: { type: Number, required: false },
	updatedAt: { type: Date, default: Date.now },
	createdAt: { type: Date, default: Date.now }
});

Tweet.methods.add = function (obj) {

	if (obj.created_at) { 
		// format to this Mai 1 2019, 4:14 AM
		this.created_at = dateFormat(obj.created_at, 'mmm dd yyyy, h:mm TT');
	};
	if (obj.user.name) { this.user.name = obj.user.name };
	if (obj.user.screen_name) { this.user.username = obj.user.screen_name };
	if (obj.user.profile_image_url) { this.user.profileImg = obj.user.profile_image_url };
	if (obj.text) { this.text = obj.text };
	if (obj.id) { this.id = obj.id };
	if (obj.id_str) { this.id_str = obj.id_str };
	if (obj.retweet_count) { this.retweet_count = obj.retweet_count };
	if (obj.favorite_count) { this.favorite_count = obj.favorite_count };
	if (obj.reply_count) { this.reply_count = obj.reply_count };
	if (obj.entities.media) { this.media = obj.entities.media };
}

Tweet.pre('save', function (next) {
	this.updatedAt = new Date();
	next();
});

module.exports = mongoose.model('Tweet', Tweet);