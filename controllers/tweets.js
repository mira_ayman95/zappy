let Tweets = require('../models/tweets');
var config = require('../config');

module.exports = {
    saveTweets: function (tweets) {
        var tweetsMap = [];

        //map tweets objects to my schema of tweets
        tweets.forEach(function (tweet) {
            let newTweet = new Tweets;
            // handle tweet object to schema 
            newTweet.add(tweet);
            tweetsMap.push(newTweet);
        });

        // add all tweets to mongo 
        Tweets.insertMany(tweetsMap, function (error, docs) {
            if (error) console.log(error);
            if (docs) console.log("Tweets saved");
        });
    },
    getAllTweets: function (opts = {}) {
        return function (req, res, next) {
            //find tweet fields needed only for viewing (projection)
            //pagination while find tweets from DB to view them in the right way 
            //page->  (records* page) to know from where to begin
            //records -> number of tweets per page
            Tweets.find({}, function (err, tweets) {
                // console.log("Tweets number getting " + tweets.length);
                if (tweets.length > 0) {
                    // doing some stuff to paginating page 
                    var start, end;
                    if (parseInt(req.params.page) != 0) start = (parseInt(req.params.page) / parseInt(req.params.records)) * parseInt(req.params.records);
                    else {
                        start = 0;
                    }
                    end = parseInt(start) + parseInt(req.params.records);
                    if (tweets.slice(start, end).length <= 0) {
                        return res.status(400).json({ error: "No more tweets in database." });
                    }
                    else {
                        return res.status(200).json({ tweets: tweets.slice(start, end) });
                    }
                } else if (err) {
                    return res.status(500).json({ error: err });
                } else {
                    return res.status(400).json({ error: "No more tweets in database." });
                }
            });
        }
    }
}

