var PORT = process.env.PORT || 4000;
global.ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var app = express();
var Config = require('./config');
var pjson = require('./package.json');
var Slack = require('./modules/RMTClient.js');
var Logger = require('./modules/mongoLogger.js');

var path = require('path');
// var bodyParser = require('body-parser');
var mongoose = require('mongoose')
var EJS = require('ejs');
var cors = require('cors');

//database connection
mongoose.Promise = global.Promise;
mongoose.connect(Config.dbURI, { useMongoClient: true });
//cors
app.use(cors());

//public & static files
app.set('views', path.join(__dirname, 'client/views'));
app.use(express.static(path.join(__dirname, 'client')));

// templating engine
app.engine('html', EJS.renderFile);
app.set('view engine', 'ejs');

// // body parser
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());


//log the time taken in each request

// app.use(Logger.routeTime());

//set port
app.set('port', PORT);

//passing routes
require('./router')(app);

//logs db connection errors
Logger.DBconnection(mongoose);

//listen to messages from slack channel
Slack.listenMessages;

// Handle 404
app.use(function (req, res) {
    res.status(404);
    res.render('pages/404', { title: '404: File Not Found' });
});
// Hande 500
app.use(function (error, req, res, next) {
    res.status(500);
    res.render('pages/500', { title: '500: Internal Server Error', error: error });
});

var server = require('http').createServer(app);
server.listen(app.get('port'), function () {
    console.log(` ################## ${pjson.name} \n ##################  ${Config.currentEnv}  \n ################## running on port : ${app.get('port')}`);
});